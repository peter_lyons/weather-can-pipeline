from datetime import datetime, timedelta
from textwrap import dedent
import pendulum

# The DAG object; we'll need this to instantiate a DAG
from airflow import DAG

# Operators; we need this to operate!
from airflow.operators.bash import BashOperator

# Toronto timezone - important to make sure cron schedules are in the right timezone!
LOCAL_TZ = pendulum.timezone('America/Toronto')
START_DATE = datetime(2022, 3, 1)
start_date_local = LOCAL_TZ.convert(START_DATE)

#END_DATE = datetime(2021,12,1)
#end_date_local = LOCAL_TZ.convert(END_DATE)

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'peter',
    'depends_on_past': False,
    'email': ['email@email.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    'wait_for_downstream': True,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}
with DAG(
    'EXECUTE_FORECAST_PULL',
    default_args=default_args,
    description='Pulls weather forecast data from weather canada api',
    schedule_interval='0 6,12 * * *', # at 6am and noon
    start_date=start_date_local,
    #end_date=end_date_local,
    catchup=False,
    tags=[''],
    max_active_runs= 1,
    concurrency=1
) as dag:
    
    airflow_date = '{{ ds_nodash }}'
    job = 'daily'

    weather_pull = BashOperator(
        task_id='weather_pull',
        depends_on_past=False,
        bash_command='./$HOME/weather-can-pipeline/scripts/run-weather-pipeline.sh ',
        retries=3,
    )

    weather_pull 