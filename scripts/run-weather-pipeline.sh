#! /bin/bash

# set pipeline to fail on first error
set -euo pipefail

# this is to connect to my own postgres instance, change for your env
#export PATH="/Applications/Postgres.app/Contents/Versions/14/bin:$PATH"

# define file location variables
weather_can="$HOME/weather-can"
weather_logs="$HOME/weather-logs"
PG_CONFIG_FILE="$HOME/creds/pg-credentials.config"

# activate environment
source "$weather_can/bin/activate"

# get the current timestamp of this application run
DATE=`date +%F_%T`
output_dir="$weather_logs/data-pull/$DATE"

# create folder in logs for this run
mkdir -p "$output_dir"

# create output file
output_file="$output_dir/forecast.csv"

# execute data pull, read forecast from stdout into a text file within the logs directory
# also print the data pull (this will show up in the airflow logs too)
tmp_val=$(python3 src/data-feed-pull.py)
echo "$tmp_val" > "$output_file" && echo "$tmp_val"

# postgres create schema/table/index if not already exists
env $(xargs < "$PG_CONFIG_FILE") psql -f "sql/create.sql"

# insert recently created file into postgres instance
env $(xargs < "$PG_CONFIG_FILE") psql -v output_file="'$output_file'" -f sql/insert.sql

# deactivate venv before finishing
deactivate

echo "data insert complete"