#! /bin/bash

# set pipeline to fail on first error
set -euo pipefail

# define file location variables
weather_can="$HOME/weather-can"
weather_logs="$HOME/weather-logs"

# destroy old env exists and create new folder
rm -r "$weather_can" || mkdir "$weather_can"

# make log directory
mkdir "$weather_logs" || echo "log directory already exits... skipping"

# create venv - overwrite on redeployment
python3 -m venv "$weather_can"

# activate venv
source "$weather_can"/bin/activate

# install pip packages
pip install --upgrade pip
pip install -r scripts/requirements.txt

deactivate

# link airflow dags
ln -s "$HOME/airflow/dags" "$HOME/weather-can-pipeline/dags"