import asyncio
import sys
from env_canada import ECWeather

# run in morning and pm
# 6am edt and ~noon edt (this is in line with commutes which transportation services seems to be ~very~ focused on)
def datetime_to_int(dt):
    return int(dt.strftime("%Y%m%d%H%M%S"))

def main():
  # establish enviro can weather object
  ec = ECWeather(coordinates=(43.67, -79.62)) # find toronto
  asyncio.run(ec.update())

  # get json (returned in list form)
  forecast = ec.daily_forecasts
  station = ec.station_id
  forecast_time = ec.forecast_time
  forecast_id_base = datetime_to_int(forecast_time)

  # today - always returns the next time forecast period
  today = forecast[0]
  today_id = "{}_1".format(forecast_id_base)
  lookahead = 'today'
  period, temp, precip = today["period"], today['temperature'], today['precip_probability']
  sys.stdout.writelines('{},{},{},{},{},{},{}\n'.format(today_id,forecast_time, lookahead, period, temp, precip, station))

  # tomorrow - if it contains 'night' in the forecast then we increment to one period earlier
  forecast_var = 2
  if 'night' in forecast[forecast_var]["period"].lower():
    forecast_var = 1

  # once time period set then get tmrw's forecast
  tomorrow = forecast[forecast_var]
  tmrw_id = "{}_2".format(forecast_id_base)
  lookahead = 'tomorrow'
  period, temp, precip = tomorrow["period"], tomorrow['temperature'], tomorrow['precip_probability']
  sys.stdout.writelines('{},{},{},{},{},{},{}'.format(tmrw_id, forecast_time, lookahead, period, temp, precip, station))

main()