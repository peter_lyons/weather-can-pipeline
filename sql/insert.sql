-- create temporary table to select all the distinct values from the weather_gc.forecast table
create temp table tmp as 
select distinct * 
from weather_gc.forecast;

-- copy the new values from the log output (via stdout) into the temp table as well
COPY tmp FROM :output_file ( FORMAT CSV, DELIMITER(',') ); 

-- now insert only the new values into the table.
INSERT INTO weather_gc.forecast 
SELECT distinct * 
FROM tmp ON CONFLICT (forecast_id) DO NOTHING;

-- remove temporary table
drop table tmp
