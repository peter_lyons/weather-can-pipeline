-- create weather schema if it does not already exist
create schema if not exists weather_gc;

-- create forecast table if not already exist.
create table if not exists weather_gc.forecast (
  forecast_id text,
	forecast_time	timestamp without time zone,
  lookahead text,
	time_period	text,
	temperature	int,
  chance_precip int,
 	station	text
);

-- create unique index that is used for preventing insertion of duplicate values.
create unique index if not exists forecast_id_index on weather_gc.forecast (forecast_id);