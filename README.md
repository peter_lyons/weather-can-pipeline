# weather-can-pipeline
Peter Lyons
April 26, 2022

## Prerequisites
- User has python3 installed
- User is running this on a unix system
- User has an airflow instance located at `~/airflow`
- User has a postgres database with `psql` command line utility installed
- User has postgres credentials stored in a file `~/creds/pg-credentials.config`. 
  Example:
  ```
  PGHOST="localhost"
  PGDATABASE=""
  PGUSER=""
  PGPASSWORD=""
  ```

- IF the user is behind a firewall, they may need to adjust the installation script to work with proxy settings.
  ```
  USERNAME=
  PASSWORD=
  python3 -m pip install --proxy=http://$USERNAME:$PASSWORD@proxy.city_name.ca:8080 --upgrade pip
  python3 -m pip install --proxy=http://$USERNAME:$PASSWORD@proxy.city_name.ca:8080 -r scripts/requirements.txt
  ```

## Deployment Steps
1. Clone repository into your home directory and name it `weather-can-pipeline`
2. Ensure prerequisites are satisfied, make changes to deploy script (`deploy_scripts/deploy-pipeline.sh`) to adjust pip proxy if needed.
3. Enter the repository folder from your terminal of choice. Execute the deployment script `./deploy_scripts/deploy-pipeline.sh`
4. Once complete the virtual environment and pip packages will be installed.
5. Navigate to airflow instance and it should be visible as `EXECUTE_FORECAST_PULL` once the application has picked up the DAG.
6. Turn on the DAG.`

## Pipeline at a Glance
### Data Feed
- A python package by the name of `env-canada` provides an robust way for accessing current and forecasted weather from environment canada.

### Parsing
- The object returned contains multiple lists for some of the following: current weather, forecasted weather, station id, forecast timestamp

### Unique Values
- It's imperative to create a unique key for this dataset, as it will help prevent duplication upon insertion of data into postgres

### Data Structure
The schema contains a unique key (forecast_id) that is a combination of the integer representation of the forecast timestamp & the addition of "_1" or "_2" depending whether it's to represent "today" or "tomorrow". Weather station was included, albeit redundant in this case, if Transportation services wants to add additional forecasting locations.

| Column Name   | Data Type                  | Desc                                                      |
|---------------|----------------------------|-----------------------------------------------------------|
| forecast_id   | text                       | unique identifier                                         |
| forecast_time | timezone without timestamp | timestamp when forecast was issued                        |
| lookahead     | text                       | whether the row value is a forecast for today or tomorrow |
| time_period   | text                       | the day of week/period (ex. Monday Night, Tuesday)        |
| temperature   | int                        | the forecasted temperature                                |
| chance_precip | int                        | the chance of precipitation (0-100)                       |
| station       | text                       | the weather station id                                    |

## Codebase
### Executing the pipeline
- Deployment achieved via script invocation
  - The script `./deploy_scripts/deploy-pipeline.sh` will create/recreate a python venv `weather_can`
  - pip will install required packages listed in `/deploy_scripts/requirements.txt`
  - directories for data logs will be created
  - symlink of airflow dag directory to `/weather-can-pipeline/dags`

### Postgres Configuration File
- Configuration file is kept outside the repository
- The user must create a file and folder in `$HOME/creds/pg-credentials.config` and add the following, changing the parameters to match their environment.
  ```
  PGHOST="host address"
  PGDATABASE="database name"
  PGUSER="username"
  PGPASSWORD="password"
  ````

### `scripts` directory
- Here is the main entrance for running the pipeline manually. Trigger the pipleine by running `./scripts/run-weather-pipeline.sh`. The shell script will set the relevant variables, activate the python virtual environment, and execute the python program to pull the data from weather canada. The python program will write the two prepared rows into `stdout` in the shell.

- The script is set to fail if one line fails, thus we should be alerted to errors if at any point something happens.

- The shell script recieves the stdout from python and returns it in the form of a variable. This variable will write the result to a log + a timestamped folder of when the execution occurred.

- Before using that variable, a sql script is called to ensure that the destination schema and table already exist, creates them if not.

- A variable is also evaluated to be the location of the output data (in csv format). It is then passed into a `psql` query as a variable denoting the location in which to execute a `COPY` operation on.

- Lastly the environment is deactivated and a message printed stating the end of the script.

### `dags` directory
- An airflow script scheduled to run twice a day, 6am and 9pm, execute the `run-weather-pipeline.sh` shell script via a bash operator.

### `sql` directory
- In the sql directory there are two files, the first contains the queries for creating the schema, table and index. 
- The second file is the query to handle the insertion of the data. 
- An external variable is passed into the script at runtime with the location of the csv file to copy into the database.

### `src` directory
- Here lies the python code. In this file the forecast is pulled and the relevant information parsed out into a text row separated by commas, it is then written to stdout. This is performed twice once for "today" and once for "tomorrow" for each forecast.

- There are two additional components in this file, one is a function to create an integer representation of the date, used in the unique id. And another is an `if` statement that will ensure that "tomorrow"'s forecast is always for the day, and not the evening. 

### `example_output` directory
- Contains screenshots of what the data looks like in a postgres database accessed through pgadmin.

  ![data sample](example_output/data_structure.png)

- System Diagram

  ![system diagram](example_output/system_diagram.png)

# Future Work
- Create a small metadata table to include geometry and lat/long of weather station, if multiple locations are used for forecasting, can analyze the differences, how it affects different locations in the city, who receives which forecast.
